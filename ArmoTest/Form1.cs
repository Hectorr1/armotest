﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArmoTest
{
    public partial class Form1 : Form
    {
        TreeNode root = new TreeNode();
        public string startpath = "";
        Thread thread;
        Thread timeThread;

        public Form1()
        {
            InitializeComponent();
            FormClosed += new FormClosedEventHandler(ClosedForm);
            Load += new EventHandler(Loader);
        }

        

        static int count;
        public class DirectoryForSearch
        {
            DirectoryInfo startpath;
            string fileName;
            string fileContent;
            TextBox SearchFileName;
            TextBox countFiles;
            TreeView treeView;

            public DirectoryForSearch(DirectoryInfo sp, string fn, string fc,  TextBox _SearchFileName, TextBox _countFiles, TreeView tv)
            {
                this.startpath = sp;
                this.fileName = fn;
                this.fileContent = fc;
                this.SearchFileName = _SearchFileName;
                this.countFiles = _countFiles;
                this.treeView = tv;
            }

            public void Search()
            {
                string p = Convert.ToString(startpath);

                try
                {
                    FileInfo[] fls = startpath.GetFiles();

                    foreach (FileInfo info in fls)
                    {
                        count++;
                        try
                        {
                            countFiles.Invoke(
                                (ThreadStart)delegate ()
                                {
                                    countFiles.Text = "Количество проверенных файлов:" + count;
                                });
                            SearchFileName.Invoke(
                                (ThreadStart)delegate ()
                                {
                                    SearchFileName.Text = info.Name;
                                });
                            if (info.Name.Contains(fileName))
                            {
                                try //если файл системый, то файл пропускается
                                {
                                    using (StreamReader s = info.OpenText())
                                    {
                                        string f = "";
                                        f = s.ReadToEnd();
                                    }
                                }
                                catch (IOException) { continue; }

                                using (StreamReader sr = info.OpenText())
                                {
                                    string s = "";
                                    if ((s = sr.ReadToEnd()).Contains(fileContent))
                                    {
                                        treeView.Invoke(
                                        (ThreadStart)delegate ()
                                        {
                                            TreeNodeCollection col = treeView.Nodes;
                                            Nodes(info.FullName, col);
                                        });
                                        Thread.Sleep(100);
                                        //Сделать вывод в дерево

                                    }
                                }
                            }
                        }
                        catch (ThreadInterruptedException) { }

                        
                    }
                    DirectoryInfo[] drs = startpath.GetDirectories();
                    if (drs.Length != 0)
                    {
                        foreach (DirectoryInfo directory in drs)
                        {
                            DirectoryForSearch dfs = new DirectoryForSearch(directory, fileName, fileContent, SearchFileName, countFiles, treeView);
                            dfs.Search();
                            if (dfs.startpath == this.startpath)
                            {
                                break;
                            }
                        }

                    }
                }
                catch (UnauthorizedAccessException) { }
                catch (IOException) { }


            }

            public void Nodes(string name, TreeNodeCollection col)
            {
                List<string> paths = new List<string>();
                paths.AddRange(name.Split('\\'));
                for (int i = 0; i < paths.Count; i++)
                {
                    int k = 0;
                    if (col.Count != 0)
                    {
                        foreach(TreeNode n in col)
                        {
                            if (n.Text == paths[i])
                            {
                                k = 1;
                                col = n.Nodes;
                                break;
                            }
                        }
                        if (k != 1)
                        {
                            TreeNode node = new TreeNode();
                            node.Text = paths[i];
                            node.Name = paths[i];
                            col.Add(node);
                            col = node.Nodes;
                        }
                    }
                    else
                    {
                        TreeNode node = new TreeNode();
                        node.Text = paths[i];
                        node.Name = paths[i];
                        col.Add(node);
                        col = node.Nodes;
                    }
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog StarDialog = new FolderBrowserDialog();
            StarDialog.Description = "Выбор папки, с которой начнется поиск";
            if (StarDialog.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = "Стартовая директория:" + StarDialog.SelectedPath;
                startpath = StarDialog.SelectedPath;
            }
            if (thread != null)
            {
                try
                {
                    thread.Abort();
                    timeThread.Abort();
                }
                catch (ThreadStateException) { }
            }
            button3.Enabled = false;
            button2.Enabled = true;
            textBox2.ReadOnly = false;
            textBox3.ReadOnly = false;
        }

        public class Timer
        {
            Stopwatch watch = new Stopwatch();
            Thread thread;
            TextBox time;
            Button pause;

            public Timer(Thread _thread, TextBox _textBox, Button _pause)
            {
                thread = _thread;
                time = _textBox;
                pause = _pause;
            }

            public void Stop()
            {
                watch.Stop();
                pause.Invoke(
                    (ThreadStart)delegate ()
                    {
                        pause.Enabled = false;
                    });
            }

            public void Start()
            {
                watch.Start();
                pause.Invoke(
                    (ThreadStart)delegate ()
                    {
                        pause.Enabled = true;
                    });

            }

            public void toStr()
            {
                this.Start();
                while (thread.IsAlive)
                {
                    time.Invoke(
                                (ThreadStart)delegate ()
                                {
                                    time.Text = Convert.ToString(watch.Elapsed);
                                });
                    Thread.Sleep(100);
                }

                this.Stop();
            }


        }
        
        private void ClosedForm(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.StartPath = startpath;
            Properties.Settings.Default.Pattern = textBox2.Text;
            Properties.Settings.Default.Text = textBox3.Text;
            Properties.Settings.Default.Save();
            Process.GetCurrentProcess().Kill();
            Application.Exit();
        }

        

        private void Loader(object sender, EventArgs e)
        {
            textBox2.Text = Properties.Settings.Default.Pattern;
            textBox3.Text = Properties.Settings.Default.Text;
            textBox1.Text = Properties.Settings.Default.StartPath;
            startpath = Properties.Settings.Default.StartPath;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                DirectoryInfo dir = new DirectoryInfo(startpath);
                DirectoryForSearch newD = new DirectoryForSearch(dir, textBox2.Text, textBox3.Text, textBox5, textBox4, treeView1);
                thread = new Thread(newD.Search);

                Timer timer = new Timer(thread, textBox6, button3);
                timeThread = new Thread(timer.toStr);
                count = 0;
                treeView1.Nodes.Clear();
                thread.Start();
                timeThread.Start();
                button2.Enabled = false;
                button3.Text = "Пауза";
                k = true;
                textBox2.ReadOnly = true;
                textBox3.ReadOnly = true;
            }

        }
        bool k = true;
        private void button3_Click(object sender, EventArgs e)
        {
            if (k == true)
            {
                thread.Suspend();
                timeThread.Suspend();
                k = false;
                button3.Text = "Продолжить";
            }
            else
            {
                thread.Resume();
                timeThread.Resume();
                k = true;
                button3.Text = "Пауза";
            }

        }
    }
}
